import firebase from './fb/firebase'; // 4.8.1
import * as firebaseconfig from './firebaseconfig';
class Fire {
  constructor() {
    //firebase.init();
    this.observeAuth();
  }


  observeAuth = () =>
    firebase.auth().onAuthStateChanged(this.onAuthStateChanged);

  onAuthStateChanged = user => {
    if (!user) {
      try {
        firebase.auth().signInAnonymously();
      } catch ({ post }) {
        alert(post);
      }
    }
  };

  get uid() {
    return (firebase.auth().currentUser || {}).uid;
  }

  get ref() {
    return firebase.database().ref('posts');
  }

  parse = snapshot => {
    const { timestamp: numberStamp, text, user } = snapshot.val();
    const { key: _id } = snapshot;
    const timestamp = new Date(numberStamp);
    const post = {
      _id,
      timestamp,
      text,
      user,
    };
    return post;
  };

  on = callback =>
    this.ref
      .limitToLast(20)
      .on('child_added', snapshot => callback(this.parse(snapshot)));

  get timestamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }
  // send the post to the Backend
  send = posts => {
    for (let i = 0; i < posts.length; i++) {
      const { text, user } = posts[i];
      const post = {
        text,
        user,
        timestamp: this.timestamp,
      };
      this.append(post);
    }
  };

  append = post => this.ref.push(post);

  // close the connection to the Backend
  off() {
    this.ref.off();
  }
}

Fire.shared = new Fire();
export default Fire;
