import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBDvk8hyDy5W-3Nop-G_33NzyQCVxxwDFM",
    authDomain: "sqbchat.firebaseapp.com",
    databaseURL: "https://sqbchat.firebaseio.com",
    projectId: "sqbchat",
    storageBucket: "sqbchat.appspot.com",
    messagingSenderId: "172328841175"
};

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();