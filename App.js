import React from 'react';

import { StyleSheet, 
  Text, 
  View, 
  TouchableOpacity,
  } 
  from 'react-native';

import { createSwitchNavigator, 
  createMaterialTopTabNavigator, 
  createStackNavigator, 
  createDrawerNavigator } 
  from 'react-navigation'

import WelcomeScreen from './screens/WelcomeScreen.js';
import SignInScreen from './screens/SignInScreen.js';
import SignUpScreen from './screens/SignUpScreen.js';
import AuthLoadingScreen from './screens/AuthLoadingScreen.js';
import SettingsScreen from './screens/SettingScreen';
// import SignUpScreen from './screens/SignUpScreen';
import FeedScreen from './screens/FeedScreen';
import ToolsScreen from './screens/ToolsScreen';
import Chat from './screens/Chat';


//import Authentication from './components/Authentication/Authentication';


import Icon from 'react-native-vector-icons/Ionicons';



console.disableYellowBox = true;


const AuthStackNavigator = createStackNavigator({

  Welcome: WelcomeScreen,
  SignIn: SignInScreen,
  SignUp: SignUpScreen,
  //App: AppDrawerNavigator,
},
{
  animationEnabled: true
}
  
)

const AppTabNavigator = createMaterialTopTabNavigator({
  Chat: {
    screen: Chat
  },
  // HomeScreen: {
  //   screen: HomeScreen,
  // },
  
  Tools: {
    screen: ToolsScreen,
  },
  Feed: {
    screen: FeedScreen,
  },
 

},
{
  tabBarPosition: 'top',
  animationEnabled: true,
  swipeEnabled:false,
  tabBarOptions: {
  upperCaseLabel: false,
  showIcon: true,
  activeBackgroundColor:'#0EB1D2',
  inactiveBackgroundColor:'grey',
  tabStyle:{
      marginTop:0,
      height :50,
      borderRightWidth:0
  },
  labelStyle: {
      fontSize: 15,
      fontWeight:'bold',
       marginTop: 0,
       color :'#ffffff'
    },
   },
  });

const AppStackNavigator = createStackNavigator({
  AppTabNavigator: {
    screen: AppTabNavigator,
    navigationOptions: ({navigation}) => ({
      title: 'Chirp',
      headerLeft: (
        <TouchableOpacity onPress= { () => navigation.toggleDrawer()}>
          <View style={{paddingHorizontal: 10}}>
            <Icon name='md-menu' size={24}/>
          </View>
        </TouchableOpacity>
      )
    })
  }
},
{
  animationEnabled: true
})

const AppDrawerNavigator = createDrawerNavigator({
  Home: AppStackNavigator,
  Logout: SettingsScreen,
  Registration:SignUpScreen,
})

export default createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Auth: AuthStackNavigator,
  App: AppDrawerNavigator,
},
{
  animationEnabled: true
})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
