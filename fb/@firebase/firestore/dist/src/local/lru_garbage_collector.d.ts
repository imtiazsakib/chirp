import { ListenSequenceNumber } from '../core/types';
import { AnyJs } from '../util/misc';
import { PersistenceTransaction } from './persistence';
import { PersistencePromise } from './persistence_promise';
import { QueryData } from './query_data';
/**
 * Persistence layers intending to use LRU Garbage collection should have reference delegates that
 * implement this interface. This interface defines the operations that the LRU garbage collector
 * needs from the persistence layer.
 */
export interface LruDelegate {
    readonly garbageCollector: LruGarbageCollector;
    /** Enumerates all the targets in the QueryCache. */
    forEachTarget(txn: PersistenceTransaction, f: (target: QueryData) => void): PersistencePromise<void>;
    getTargetCount(txn: PersistenceTransaction): PersistencePromise<number>;
    /**
     * Enumerates sequence numbers for documents not associated with a target.
     * Note that this may include duplicate sequence numbers.
     */
    forEachOrphanedDocumentSequenceNumber(txn: PersistenceTransaction, f: (sequenceNumber: ListenSequenceNumber) => void): PersistencePromise<void>;
    /**
     * Removes all targets that have a sequence number less than or equal to `upperBound`, and are not
     * present in the `activeTargetIds` set.
     *
     * @return the number of targets removed.
     */
    removeTargets(txn: PersistenceTransaction, upperBound: ListenSequenceNumber, activeTargetIds: ActiveTargets): PersistencePromise<number>;
    /**
     * Removes all unreferenced documents from the cache that have a sequence number less than or
     * equal to the given `upperBound`.
     *
     * @return the number of documents removed.
     */
    removeOrphanedDocuments(txn: PersistenceTransaction, upperBound: ListenSequenceNumber): PersistencePromise<number>;
}
/**
 * Describes an object whose keys are active target ids. We do not care about the type of the
 * values.
 */
export declare type ActiveTargets = {
    [id: number]: AnyJs;
};
/** Implements the steps for LRU garbage collection. */
export declare class LruGarbageCollector {
    private readonly delegate;
    constructor(delegate: LruDelegate);
    /** Given a percentile of target to collect, returns the number of targets to collect. */
    calculateTargetCount(txn: PersistenceTransaction, percentile: number): PersistencePromise<number>;
    /** Returns the nth sequence number, counting in order from the smallest. */
    nthSequenceNumber(txn: PersistenceTransaction, n: number): PersistencePromise<ListenSequenceNumber>;
    /**
     * Removes targets with a sequence number equal to or less than the given upper bound, and removes
     * document associations with those targets.
     */
    removeTargets(txn: PersistenceTransaction, upperBound: ListenSequenceNumber, activeTargetIds: ActiveTargets): PersistencePromise<number>;
    /**
     * Removes documents that have a sequence number equal to or less than the upper bound and are not
     * otherwise pinned.
     */
    removeOrphanedDocuments(txn: PersistenceTransaction, upperBound: ListenSequenceNumber): PersistencePromise<number>;
}
