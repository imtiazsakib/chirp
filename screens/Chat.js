// @flow
import React from 'react';
import { GiftedChat } from 'react-native-gifted-chat'; 

import Fire from '../Fire';

import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

type Props = {
  email?: string,
};

class Chat extends React.Component<Props> {

  static navigationOptions = ({ navigation }) => ({
    title: (navigation.state.params || {}).email || 'Chat',
  });

  state = {
    messages: [],
  };

  get user() {
    return {
      email: this.props.navigation.state.params.email,
      _id: Fire.shared.uid,
    };
  }

  render() {
    return (

      
      <GiftedChat
        messages={this.state.messages}
        onSend={Fire.shared.send}
        user={{
          _id: 1, // sent messages should have same user._id
        }} 
        alwaysShowSend='True' 
        placeholder = 'Type a message...'
        
      />

    );
  }

  componentDidMount() {
    Fire.shared.on(message =>
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, message),
      }))
    );
}
  componentWillUnmount() {
    Fire.shared.off();
  }
}

export default Chat;
