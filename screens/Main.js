import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View, Button,
} from 'react-native';

class Main extends React.Component {
  static navigationOptions = {
    title: 'Chirp',
  };

  state = {
    name: '',
  };

  onPress = () =>
    this.props.navigation.navigate('Chat', { name: this.state.name });

  // onPressTools = () =>
  //   this.props.navigation.navigate('Tools');

  // onChangeText = name => this.setState({ name });

  render() {
    return (
      <View>
        {/* <Text style={styles.title}>Enter your name:</Text>
        <TextInput
          style={styles.nameInput}
          placeHolder="Abdul Karim"
          onChangeText={this.onChangeText}
          value={this.state.name}
        /> */}

      

    </View>
       
        <Button
          onPress={this.onPress}
          title="Start Chatting"
          color="purple"
          accessibilityLabel="Learn more"
        />

        {/* <Button
          onPress={this.onPressTools}
          title="Tools"
          color="black"
          accessibilityLabel="Learn more"
        /> */}

      </View>
    );
  }
}

const offset = 24;
const styles = StyleSheet.create({
  title: {
    marginTop: offset,
    marginLeft: offset,
    fontSize: offset,
  },
  nameInput: {
    height: offset * 2,

    margin: offset,
    paddingHorizontal: offset,
    borderColor: 'black',
    borderWidth: 1,
  },
  buttonText: {
    marginLeft: offset,
    fontSize: offset,
  },
});

export default Main;
