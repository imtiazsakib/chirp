import React, { Component } from "react";
import { 
    View,
    Text,
    StyleSheet,
    Button,
    AsyncStorage
} from "react-native";


class SignInScreen extends Component {

    signIn = async () => {
        await AsyncStorage.setItem('userToken', 'sakib')
        this.props.navigation.navigate('AuthLoading')
    }
    render() {
        return (
            <View style={styles.container}>
            
            <Text style={{marginBottom:50}}>
                New user added Successfully!
            </Text>
            
            <Button  title='Sign In' onPress={this.signIn}/>
            
            </View>
        );
    }
}
export default SignInScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'lightyellow'
    }
});