import React, { Component } from "react";
import { 
    View,
    Text,
    StyleSheet,
    
} from "react-native";

import {Container, Content, Header, Form, Input, Item, Button, Label} from 'native-base'


class WelcomeScreen extends Component {
    render() {
        return (
    <View style={styles.container}>
        <Button title='Sign In' onPress={()=>this.props.navigation.navigate('SignIn')}/>
        <Button title='Sign Up' onPress={()=>this.props.navigation.navigate('SignUp')}/>
    </View>
);
      }
}
export default WelcomeScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',    
      justifyContent: 'center',
      
    },
  
    
  
    
  });

// return (
//     <View style={styles.container}>
//         <Button title='Sign In' onPress={()=>this.props.navigation.navigate('SignIn')}/>
//         <Button title='Sign Up' onPress={()=>this.props.navigation.navigate('SignUp')}/>
//     </View>
// );