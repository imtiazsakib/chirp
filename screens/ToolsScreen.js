import React, { Component } from "react";
import { StatusBar, Image, TouchableOpacity } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";

import { 
    View,
    Text,
    StyleSheet
} from "react-native";

class ToolsScreen extends Component {
    render() {
        return (

            

            <View style={styles.container}>
                <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
                

                <Grid style={styles.container}>
                    <Col>
                        <Row>
                            <TouchableOpacity>
                            <Image 
                            source={require('../assets/notepad-icon.png')}
                            style={{width: 80, height: 80}}                                 
                            />
                            </TouchableOpacity>
                        </Row>

                        <Row>
                            <TouchableOpacity>
                            <Image 
                            source={require('../assets/loc.png')}
                            style={{width: 80, height: 80}}                                 
                            />
                            </TouchableOpacity>
                        </Row>

                        <Row>
                            <TouchableOpacity>
                            <Image 
                            source={require('../assets/pocket.png')}
                            style={{width: 80, height: 80}}                                 
                            />
                            </TouchableOpacity>
                        </Row>
                    </Col>

                    <Col>
                        <Row>
                            <TouchableOpacity>
                            <Image 
                            source={require('../assets/poll-128.png')}
                            style={{width: 80, height: 80}}                                 
                            />
                            </TouchableOpacity>
                        </Row>

                        <Row>
                            <TouchableOpacity>
                            <Image 
                            source={require('../assets/calender.png')}
                            style={{width: 80, height: 80}}                                 
                            />
                            </TouchableOpacity>
                        </Row>

                        <Row>
                            <TouchableOpacity>
                            <Image 
                            source={require('../assets/Add-icon.png')}                            
                            style={{width: 80, height: 80}}                                 
                            />
                            </TouchableOpacity>
                        </Row>
                    </Col>

                    <Col>
                        <Row>
                            <TouchableOpacity>
                            <Image 
                            source={require('../assets/download.jpeg')}
                            style={{width: 80, height: 80}}                                 
                            />
                            </TouchableOpacity>
                        </Row>

                        <Row>
                            <TouchableOpacity>
                            <Image 
                            source={require('../assets/About-256.png')}
                            style={{width: 80, height: 80}}                                 
                            />
                            </TouchableOpacity>
                        </Row>

                        <Row>
                            <TouchableOpacity>
                            <Image 
                            source={require('../assets/download (2).png')}
                            style={{width: 80, height: 80}}                                 
                            />
                            </TouchableOpacity>
                        </Row>
                    </Col>

                </Grid>
            </View>
        );
    }
}
export default ToolsScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingTop: 10,
    },
    image: {
        width: '80%',
        height: '80%',
        borderRadius: 10,
      },
});