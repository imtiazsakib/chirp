// @flow
import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat'; 
import {RkButton,RkTextInput,RkText, RkCard} from 'react-native-ui-kitten';
import Icon from 'react-native-vector-icons/Ionicons';


import Fire from '../Firepost';

import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = post => {
  if (post.indexOf('Setting a timer') <= -1) {
    _console.warn(post);
  }
};

type Props = {
  email?: string,
};

class Feed extends React.Component<Props> {

  static navigationOptions = ({ navigation }) => ({
    title: (navigation.state.params || {}).email || 'Feed',
  });

  state = {
    posts: [],
  };

  get user() {
    return {
      email: this.props.navigation.state.params.email,
      _id: Fire.shared.uid,
    };
  }

  render() {
    return (
      <ScrollView style={styles.container}>

        <RkText> Create a new post: </RkText>
        <RkTextInput
          labelStyle={{color: 'gray'}}          
          inputStyle={{
              backgroundColor: 'lightgray',
              color: 'black',
              height: 80,
        }}/>
        <RkButton 
        style={{marginRight: 10, 
                align:'right',
                height: 40,
                width: 80,
                
                }} >Post</RkButton>

        
        <RkCard style={{marginTop: 10}}>
          <View rkCardHeader>
            <Text style={styles.titleText}>FYP Showcase Starting Today</Text>
          </View>
          <Image rkCardImg source={require('../assets/kict.jpg')}/>
          <View rkCardContent>
            <Text >Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui</Text>
          </View>
          <View rkCardFooter>
            <Text>voluptatem accusantium doloremque laudantium, totam rem aperiam</Text>
          </View>
        </RkCard>

        <RkCard style={{marginTop: 10}}>
          <View rkCardHeader>
            <Text style={styles.titleText}>IIUM ICPC Team Won the Nationals</Text>
          </View>
          <Image rkCardImg source={require('../assets/icpc.jpeg')}/>
          <View rkCardContent>
            <Text >Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui</Text>
            
          </View>
          <View rkCardFooter>
            <Text>voluptatem accusantium doloremque laudantium, totam rem aperiam</Text>
          </View>
        </RkCard>
      </ScrollView>
    );
  }






  componentDidMount() {
    Fire.shared.on(post =>
      this.setState(previousState => ({
        posts: GiftedChat.append(previousState.posts, post),
      }))
    );
}
  componentWillUnmount() {
    Fire.shared.off();
  }
}



export default Feed;


const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  container: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
});
