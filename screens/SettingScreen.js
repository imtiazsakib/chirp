import React, { Component } from "react";
import { 
    View,
    Text,
    StyleSheet,
    AsyncStorage,
    Button,
    ActivityIndicator,
} from "react-native";



class SettingScreen extends Component {

    signOut = async () => {
         AsyncStorage.clear()
         this.props.navigation.navigate('AuthLoading')
    }

    
    
    render() {
        return (
            
            <View style={styles.container}>
                <Text> Are you sure? </Text>
               <Button style={{marginTop: 10}} title= 'Sign Out' onPress = { () => this.signOut()} />
               <Button style={{marginTop: 10}} title= 'Cancel' onPress = { () => this.props.navigation.navigate('Chat')} />
            </View>
        );
    }
}
export default SettingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        
    }
});